import { Component, OnInit, Inject } from '@angular/core';
import { LeaderService } from '../services/leader.service';
import { Leader } from '../shared/leader';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  leader:Leader[];

  constructor(private leaderService: LeaderService,
    @Inject('BaseURL') private BaseURL) { }

  
  ngOnInit() {

    this.leaderService.getLeaders()
    .subscribe((leader) => this.leader = leader)
    console.log(this.leader)
  }

}
