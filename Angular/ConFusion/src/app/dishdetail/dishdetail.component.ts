import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { Comment } from '../shared/comment';  
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, state, style, animate, transition } from '@angular/animations';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  animations: [
    trigger('visibility', [
      state('show',style({
        transform: 'scale(1.0)',
        opacity:1
      })),
      state('hidden',style({
        transform: 'scale(0.5)',
        opacity:0
      })),
      transition('* => *', animate('0.5s ease-in-out'))
    ])
  ]
})
export class DishdetailComponent implements OnInit {


  commentForm: FormGroup;
  dishcopy: Dish;
  visibility = 'shown';
  comment: Comment;
  dish:Dish;
  dishIds:string[];
  prev:string;
  next:string;  
  errMess:string;
  @ViewChild('fform') commentFormDirective;
    //comments:this.dish.comments;
    
    formErrors = {
      'author': '',
      'comment': '',
    };  

    validationMessages = {
      'author': {
        'required':      'Name is required.',
        'minlength':     'Name must be at least 2 characters long.',
        'maxlength':     'FirstName cannot be more than 25 characters long.'
      },
      'comment': {
        'required':      'Comment is required.',
        'minlength':     'Comment must be at least 2 characters long.',
        'maxlength':     'Comment cannot be more than 100 characters long.'
      },
    };
  
    createForm() {
      this.commentForm = this.fb.group({
        author:['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
        comment:['', [Validators.required,Validators.minLength(2), Validators.maxLength(100)]],
        rating:[0]
      });

      this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  
    }

  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private BaseURL,
    ) {
      console.log(this.dish)
      this.createForm();
      
  }

  ngOnInit() {
    this.dishService.getDishIds()
    .subscribe((dishIds) => this.dishIds = dishIds);
    
    //let id =  this.orute.spanshot.params['id'];
    this.route.params.pipe(switchMap((params:Params)=>{this.visibility='hidden'; return this.dishService.getDish(params['id']);}))
    //this.dishService.getDish(id)
    .subscribe((dish) => {this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id), this.visibility = 'shown';},
    errmess => this.errMess = <any>errmess
    );

  }

  onValueChanged(data?: any) {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.comment = this.commentForm.value;
    
    this.comment.date = new Date().toISOString();
    this.dishcopy.comments.push(this.comment);
    this.dishService.putDish(this.dishcopy)
    .subscribe(dish => {
      this.dish = dish; this.dishcopy = dish;
    },
    errmess => {
      this.dish = null; this.dishcopy = null; this.errMess = <any>errmess});
    this.dishService.getDishIds()
    .subscribe((dishIds) => this.dishIds = dishIds)

    this.commentForm.reset({
      comment:'',
      author:'',
      rating:0,
    });
    this.commentFormDirective.resetForm();

  }

  setPrevNext(dishIds: string) {

    const index = this.dishIds.indexOf(dishIds);
    console.log(this.dishIds.length)
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void{
  this.location.back()
  } 


}
