import { Injectable } from '@angular/core';
import { Promotion } from '../shared/promotion';
import { PROMOTIONS } from '../shared/promotions';
import { P } from '@angular/core/src/render3';
import { Observable , of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {


getPromotions(): Observable<Promotion[]>{
  return this.http.get<Promotion[]>(baseURL + 'promotions');
}

getPromotion(id: string): Observable<Promotion>{
  return this.http.get<Promotion>(baseURL + 'promotions/' + id);
}

getFeaturePromotion(): Observable<Promotion> {
  return this.http.get<Promotion[]>(baseURL + 'promotions?featured=true')
  .pipe(map(promotion => promotion[0]))
}

  constructor(private http: HttpClient) { }
}

