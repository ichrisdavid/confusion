import { Component, OnInit, ViewChild, AfterViewInit, Inject  } from '@angular/core';
import { Dish } from '../shared/dish';
import { Comment } from '../shared/comment';
import { DishdetailComponent } from '../dishdetail/dishdetail.component';
import  { DishService } from '../services/dish.service';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {


  dishes: Dish[];
  errMess:string;

  // data = ''
  // comments:Comment[];


  // //selectedDish: Dish;

  // onSelect(dish: Dish){
  //   this.selectedDish = dish;
  //   console.log(this.selectedDish)
  // }
 
  constructor(private dishService: DishService,
    @Inject('BaseURL') private BaseURL) {}

  ngOnInit() {
    this.dishService.getDishes()
    .subscribe((dishes) => this.dishes = dishes,
    errmess => this.errMess = <any>errmess);
  }

}
